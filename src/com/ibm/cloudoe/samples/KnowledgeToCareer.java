package com.ibm.cloudoe.samples;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;

/**
 * Servlet implementation class KnowledgeToCareer
 */
@WebServlet("/KnowledgeToCareer")
public class KnowledgeToCareer extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KnowledgeToCareer() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
		response.getWriter().println("Error 405 Get Method Not Allowed!");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//check input correctness
		/*
		if(request.getContentType().toLowerCase().indexOf("application/json") == -1) {
			response.setStatus(HttpServletResponse.SC_UNSUPPORTED_MEDIA_TYPE);
			response.getWriter().println("Media type not supported. Need JSON");
			return;
		}
		*/

		BufferedReader br = request.getReader();
		StringBuffer sb = new StringBuffer();
		String line;
		while((line = br.readLine()) != null) {
			sb.append(line);
		}

		//JSONObject json = new JSONObject(sb.toString());
		//JSONArray jsonArray = json.getJSONArray("courseList");
		//for(Object obj: jsonArray) {
		//	String course = (String)obj;
		//}

		//hard code now
		String hardCodeJsonString  = 
			"{\"jobs\":[{\"text\":\"game designer\",\"skills\":[\"play game\",\"quit cc\"]},{\"text\":\"president\",\"skills\":[\"day dreaming\",\"keep day dreaming\"]}],\"courseList\":[{\"text\":\"math\",\"children\":[\"linear algebra\",\"discrete math\"]},{\"text\":\"computer science\",\"children\":[\"algorithm\",\"data structure\"]}]}";
		
		response.setStatus(HttpServletResponse.SC_OK);
		response.setContentType("application/JSON");
		response.getWriter().println(hardCodeJsonString);
	}
	
	/*
	public static String getBody(HttpServletRequest request) throws IOException {

	    String body = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    BufferedReader bufferedReader = null;

	    try {
	        InputStream inputStream = request.getInputStream();
	        if (inputStream != null) {
	            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	            char[] charBuffer = new char[128];
	            int bytesRead = -1;
	            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
	                stringBuilder.append(charBuffer, 0, bytesRead);
	            }
	        } else {
	            stringBuilder.append("");
	        }
	    } catch (IOException ex) {
	        throw ex;
	    } finally {
	        if (bufferedReader != null) {
	            try {
	                bufferedReader.close();
	            } catch (IOException ex) {
	                throw ex;
	            }
	        }
	    }

	    body = stringBuilder.toString();
	    return body;
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		PrintWriter printWriter = response.getWriter();
		response.setContentType("application/json");
		String jsonString =  getBody(request);
		try {
			JSONObject obj = new JSONObject(jsonString);
			JSONArray courseArray = obj.getJSONArray("courseList");
			
			for (int index=0;index<courseArray.length();index++){
				String content = (String) courseArray.get(index);
				System.out.println(content);
			}
			Map<String, Object> config = new HashMap<String, Object>();
	        //if you need pretty printing
	        //config.put("javax.json.stream.JsonGenerator.prettyPrinting", Boolean.valueOf(true));
		        
			JsonBuilderFactory factory = Json.createBuilderFactory(config);
			JsonObject json = factory.createObjectBuilder()
				.add("knowledge", factory.createArrayBuilder()
					.add(factory.createObjectBuilder()
			            .add("name", "Discrete Math")
			            .add("children",factory.createArrayBuilder().add(
			            	factory.createObjectBuilder()
					            .add("name", "Operating Systems")
			            	).add(
			            	factory.createObjectBuilder()
					            .add("name", "Computer Graphics")
			            	)
			            )
			        )
			    ).add("career", factory.createArrayBuilder()
			    	.add(factory.createObjectBuilder()
				        .add("name", "Game Developer")
						.add("skills", factory.createArrayBuilder()
								.add("game").add("design")
						    )
					)
				).build();
			 printWriter.append(json.toString());				
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	*/

}
