var Graph = function() {}

Graph.prototype.post = function (url, data, callback) {
  /*
    var data = {
        "jobs":[
            {"text":"game designer","skills":["play game","quit cc"]},
            {"text":"president","skills":["day dreaming","keep day dreaming"]}
        ],
        "courseList":[
            {"text":"math","children":["linear algebra","discrete math"]},
            {"text":"computer science","children":["algorithm","data structure"]}
        ]
    }
    this.demo = data.courseList;
    this.jobs = data.jobs;
    // sep
    $.post(url, {}, function( data ) {
    	console.log(data);
        this.demo = data.courseList;
        this.jobs = data.jobs;
    });
    */
    console.log(data)
	$.ajax({
		'type':"POST",
		'url':url,
		'data': JSON.stringify(data),
		'dataType': 'json',
		success: function(result) {
			console.log(result);
			this.demo = result.courseList;
			this.jobs = result.jobs;
			callback(result);
		}
	})
}




Graph.prototype.root = function() {
    return this.demo;
};

Graph.prototype.joblist = function() {
    return this.jobs;
}
