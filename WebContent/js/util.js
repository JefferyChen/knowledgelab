var Util = function(ctx) {
    this._ctx = ctx;
}

Util.prototype.drawBlueRect = function(pt) {
    w = 30;
    this._ctx.fillStyle = "blue";
    this._ctx.fillRect(pt.x-w/2, pt.y-w/2, w, w);
}

Util.prototype.drawSkill = function(pt, skillname, color) {
    // prepare text
    this._ctx.font = "18px Arial";
    var textSize = this._ctx.measureText(skillname)

    // backup
    backupW = this._ctx.lineWidth;
    backupC = this.strokeStyle;

    // draw rect
    this._ctx.fillStyle = "#1B9AF7"
    this._ctx.fillRect(pt.x - textSize.width/2 - 7, pt.y-11, textSize.width+14, 28)
    this._ctx.beginPath();
    if (color == 'normal')
        this._ctx.strokeStyle = '#0084B4';
    else
        this._ctx.strokeStyle = color
    this._ctx.fillStyle = "white";
    this._ctx.rect(pt.x - textSize.width/2 - 7, pt.y-11, textSize.width+14, 28)
    this._ctx.lineWidth = 2;
    // line color
    this._ctx.stroke();

    // draw text
    this._ctx.fillText(skillname, pt.x-textSize.width/2, pt.y+9)

    // retrive
    this._ctx.lineWidth = backupW;
    this._ctx.strokeStyle = backupC;
}

Util.prototype.drawPeople = function(pt, radius, type) {
    if (type == "Ghost") {
        return;
    }

    // backup
    backupW = this._ctx.lineWidth;
    backupC = this.strokeStyle;

    // draw circle
    this._ctx.beginPath();
    this._ctx.arc(pt.x, pt.y, radius, 0, 2*Math.PI)
    this._ctx.fillStyle = "white"
    this._ctx.fill();
    this._ctx.lineWidth = 2;
    // line color
    this._ctx.strokeStyle = 'red';
    this._ctx.closePath();
    this._ctx.stroke();

    // draw text
    this._ctx.fillStyle = "black";
    this._ctx.font = "18px Arial";
    var textSize = this._ctx.measureText("You")
    this._ctx.fillText("You", pt.x-textSize.width/2, pt.y+9)

    // retrive
    this._ctx.lineWidth = backupW;
    this._ctx.strokeStyle = backupC;
}