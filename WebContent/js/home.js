windowHeight = 0;
windowWidth = 0;

$(function(){
    function resize() {
        windowWidth = $(window).width();
        windowHeight = $(window).height() - 50;
        $(".segment").height(windowHeight);
        $(".segment").width(windowWidth);
        $(".jobs").height(windowHeight*0.4);
        $("#viewport").attr("width",windowWidth);
        $("#viewport").attr("height",windowHeight - windowHeight*0.4);
    }
    resize();

    $(window).resize(function(){
        resize();
    })

    $("#importbtn").click(function(){
        $(".importphase").hide()
        $("#coursetable").toggleClass('hide')
    })

    $("#genbtn").click(function(){
        var graph = new Graph()
        console.log(graph)
        var data = {"courseList":[]};
        var selectlist = $(".selected");
        for (var i = 0; i < selectlist.length; i ++) {
            data['courseList'].push($(selectlist[i]).text());
        }
        graph.post("http://knowledgelab1.mybluemix.net/KnowledgeToCareer", data, function(result){
        	console.log("callback", result);
        	graph.demo = result.courseList;
        	graph.jobs = result.jobs;
        	start(graph);
        });
        $('body').animate({
            scrollTop: 3000
        }, 2000);
        return false;
    })

    $("#coursetable tr td div").click(function() {
        $(this).toggleClass("selected");
        $(this).toggleClass("button-glow");
    })

    $(".joba").mouseenter(function(){
        var coulist = $(this).attr('data-course').split(' ');
        sysColor(coulist, 'orange');
    })
    $(".joba").mouseleave(function(){
        var coulist = $(this).attr('data-course').split(' ');
        sysDeColor(coulist);
    })

})
