$(function(){

    function resize() {
        windowWidth = $(window).width();
        windowHeight = $(window).height() - 50;
        $(".segment").height(windowHeight);
        $(".segment").width(windowWidth);
        $(".jobs").height(windowHeight*0.4);
        $("#viewport").attr("width",windowWidth);
        $("#viewport").attr("height",windowHeight - windowHeight*0.4);
        // 
        var content = $('#logo');
        $(content).css('margin-top', (windowHeight-$(content).height())/2-100);
    }
    resize();

    $(window).resize(function(){
        resize();
    })

    $("#searchjobs").click(function(){
    	windowHeight = $(window).height() - 120;
    	$(".segment").height(windowHeight-50);
        $('body').animate({
            scrollTop: 450
        }, 500);
        return false;
    })
})