//
//  main.js
//
//  A project template for using arbor.js
//

ssys = null;
nodeList = null;
jobList = null;

canvas_scale = 1;
canvas_offset = [0,0];
canvas_base = [0,0];

gallery = null;

function getChildren(name) {
  nodeList.forEach(function(node){
    if (node.text === name) {
      console.log("get children", node.children)
      return node.children.slice();
    }
  })
}

function sysAddChildren(father) {
  nodeList.forEach(function(node){
    if (node.text == father) {
      ll = node.children;
      for(var i=0; i<ll.length; i++) {
        if (ssys.getNode(ll[i])) continue;
        // add node to graph
        ssys.addNode(ll[i], {text:ll[i], expand:false, color:'normal'});
        ssys.addEdge(ssys.getNode(father), ssys.getNode(ll[i]))
        
        // add item to gallery
        for (var j = 0; j < jobList.length; j ++) {
          if(jobList[j].skills.indexOf(ll[i]) != -1){
            gallery.add(jobList[j]);
          }
        }

      }
    }
  })
}

// red, origin, normal
function sysColor(nodes, color) {
  nodes.forEach(function(nodename){
    var nod = ssys.getNode(nodename)
    if (nod) {
      nod.data.color = color;
      nod.p.x = nod.p.x + 0.01;
    }
  })
}

function sysDeColor(nodes) {
  nodes.forEach(function(nodename) {
    var nod = ssys.getNode(nodename)
    if (nod) {
      nod.data.color = 'normal'
      nod.p.x = nod.p.x + 0.01;
    }
  })
}

function scaleUp() {
  canvas_scale = canvas_scale * 1.02
  var canvas = $("#viewport").get(0)
  canvas_offset[0] = (0.5) * canvas.width * (canvas_scale-1)
  canvas_offset[1] = (0.5) * canvas.height * (canvas_scale-1)
  ssys.getNode('a').p.x = ssys.getNode('a').p.x+0.01
}

function scaleDown() {
  canvas_scale *= 0.98;
  if (canvas_scale < 0.7) return;
  canvas_offset[0] = (0.5) * canvas.width * (canvas_scale-1)
  canvas_offset[1] = (0.5) * canvas.height * (canvas_scale-1)
  ssys.getNode('a').p.x = ssys.getNode('a').p.x+0.01
}

function scrollLeft() {
  canvas_base[0] -= 4;
}

function scrollRight() {
  canvas_base[0] += 4;
}

function getMouse(e, pos) {
  return arbor.Point((e.pageX-pos.left+canvas_offset[0])/canvas_scale, (e.pageY-pos.top+canvas_offset[1])/canvas_scale);
}

function start(graph) {
  nodeList = graph.root();
  jobList = graph.joblist();
  root = nodeList[0];
  var rn = ssys.addNode(root.text, {mass:.25, text: root.text, expand: false, color:"normal"});
  for (var j = 0; j < jobList.length; j ++) {
    if (jobList[j].skills.indexOf(root.text) != -1)
      gallery.add(jobList[j])
  }
  ssys.addEdge(ssys.getNode('a'), rn)
}

(function($){

  var Renderer = function(canvas){
    var canvas = $(canvas).get(0)
    var ctx = canvas.getContext("2d");
    var particleSystem
    var util = new Util(ctx);

    var that = {
      init:function(system){
        //
        // the particle system will call the init function once, right before the
        // first frame is to be drawn. it's a good place to set up the canvas and
        // to pass the canvas size to the particle system
        //
        // save a reference to the particle system for use in the .redraw() loop
        particleSystem = system

        // inform the system of the screen dimensions so it can map coords for us.
        // if the canvas is ever resized, screenSize should be called again with
        // the new dimensions
        particleSystem.screenSize(canvas.width, canvas.height) 
        particleSystem.screenPadding(80) // leave an extra 80px of whitespace per side
        
        // set up some event handlers to allow for node-dragging
        that.initMouseHandling()
      },
      
      redraw:function(){
        // 
        // redraw will be called repeatedly during the run whenever the node positions
        // change. the new positions for the nodes can be accessed by looking at the
        // .p attribute of a given node. however the p.x & p.y values are in the coordinates
        // of the particle system rather than the screen. you can either map them to
        // the screen yourself, or use the convenience iterators .eachNode (and .eachEdge)
        // which allow you to step through the actual node objects but also pass an
        // x,y point in the screen's coordinate system
        // 
        ctx.translate(-canvas_offset[0]+canvas_base[0], -canvas_offset[1]+canvas_base[1])
        ctx.scale(canvas_scale, canvas_scale)

        ctx.fillStyle = "white"
        ctx.fillRect(0,0, canvas.width, canvas.height)

        // var img = new Image()
        // img.src = "./img/background.jpg"
        // ctx.drawImage(img, 0, 0, canvas.width, canvas.height)
        
        particleSystem.eachEdge(function(edge, pt1, pt2){
          // edge: {source:Node, target:Node, length:#, data:{}}
          // pt1:  {x:#, y:#}  source position in screen coords
          // pt2:  {x:#, y:#}  target position in screen coords

          // draw a line from pt1 to pt2
          if(edge.source == ssys.getNode('ghost')) return;

          ctx.strokeStyle = "rgba(0,0,0, .333)"
          ctx.lineWidth = 1
          ctx.beginPath()
          ctx.moveTo(pt1.x, pt1.y)
          ctx.lineTo(pt2.x, pt2.y)
          ctx.stroke()
        })

        particleSystem.eachNode(function(node, pt){
          // node: {mass:#, p:{x,y}, name:"", data:{}}
          // pt:   {x:#, y:#}  node position in screen coords

          // draw a rectangle centered at pt
          var w = 10
          var nodeH = w;
          var nodeW = w;
          ctx.fillStyle = "orange";
          if (node.data.text != null) {
            if (typeof node.data.type != 'undefined')
              util.drawPeople(pt, 30, node.data.type) 
            else {
              if ( typeof node.data.color === 'undefined')
                alert("no color")
              util.drawSkill(pt, node.data.text, node.data.color)
            }
          }
        })     


        ctx.scale(1/canvas_scale, 1/canvas_scale)     
        ctx.translate(canvas_offset[0]-canvas_base[0], canvas_offset[1]-canvas_base[1])   
      },
      
      initMouseHandling:function(){
        // no-nonsense drag and drop (thanks springy.js)
        var dragged = null;

        // set up a handler object that will initially listen for mousedowns then
        // for moves and mouseups while dragging
        var handler = {
          touched:function(e){

            var pos = $(canvas).offset();
            _mouseP = getMouse(e, pos)
            dragged = particleSystem.nearest(_mouseP);
            
            // check is near
            clickP = particleSystem.fromScreen(_mouseP), 
            nodeP = dragged.node.p
            dx = clickP.x - nodeP.x;
            dy = clickP.y - nodeP.y;
            if (dx*dx + dy*dy > 0.3) {
              return false;
            }
            console.log("clicked")

            if (dragged && dragged.node !== null){
              // while we're dragging, don't let physics move the node
              dragged.node.fixed = true
            }

            $(canvas).bind('mousemove', handler.dragged)
            $(window).bind('mouseup', handler.dropped)

            return false
          },
          dragged:function(e){

            var pos = $(canvas).offset();
            var s = getMouse(e, pos)

            if (dragged && dragged.node !== null){
              var p = particleSystem.fromScreen(s)
              dragged.node.p = p
            }

            return false;
          },

          dropped:function(e){

            if (dragged===null || dragged.node===undefined) return
            if (dragged.node !== null) dragged.node.fixed = false
            dragged.node.tempMass = 1000
            dragged = null
            $(canvas).unbind('mousemove', handler.dragged)
            $(window).unbind('mouseup', handler.dropped)
            _mouseP = null
            return false
          },

          clicked:function(e) {
            var pos = $(canvas).offset();
            _mouseP = getMouse(e, pos)
            obj = particleSystem.nearest(_mouseP);
            var node = obj.node;
            sysAddChildren(node.data.text, 'normal');
          }
        }
        
        // start listening
        $(canvas).mousedown(handler.touched);
        $(canvas).click(handler.clicked);
      }
      
    }
    return that
  }    

  $(document).ready(function(){
    var sys = arbor.ParticleSystem(30, 600, 0.1) // create the system with sensible repulsion/stiffness/friction
    sys.parameters({gravity:true}) // use center-gravity to make the graph settle nicely (ymmv)
    sys.renderer = Renderer("#viewport") // our newly created renderer will have its .init() method called shortly by sys...

    // add some nodes to the graph and watch it go...
    sys.addNode('a', {mass:.25, text:"You", type: "people"})
    ssys = sys;
    sys.addNode('ghost', {mass:1, text:"Ghost", type: "Ghost"})
    sys.addEdge(sys.getNode('ghost'), sys.getNode('a'))

    gallery = new Gallery()

    // key event
    $(window).keydown(function (e) {
      if (e.which === 87) {
          scaleUp();
      } else if (e.which === 83) {
          scaleDown();
      } else if (e.which === 65) {
          scrollLeft();
      } else if (e.which === 68) {
          scrollRight();
      }
    })
    
  })

})(this.jQuery)