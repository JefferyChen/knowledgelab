var Gallery = function() {
    this._gallery = $(".jobs")[0]
    this.shown = [];
}

Gallery.prototype.relocal = function() {
    var jobs = $(".job")
    var width = $(jobs[0]).width();
    for (var i = 0; i < jobs.length; i ++) {
        $(jobs[i]).css("left",i * (width+5))
        $(jobs[i]).removeClass("mi");
    }
};

Gallery.prototype.add = function(job) {
    console.log("gallery add", job.text)
    if (this.shown.indexOf(job.text) != -1) return;
    if (job.image === "some link")
        job.image = "http://knowledgelab1.mybluemix.net/img/SoftwareEngineer.jpg"
    $(this._gallery).prepend('<div class="job mi" style="left:0; top:0px"><a href="http://knowledgelab1.mybluemix.net/wiki.html" class="joba" data-course="'+job.skills.join(' ')+'"><div class="cover"><h3>' +job.text+ '</h3><p>'+job.description+'</p></div><img width="400" height="100%" src="'+job.image+'" alt="Programmer"></a></div>');
    this.shown.push(job.text)
    this.relocal();
    
    $(".joba").mouseenter(function(){
        var coulist = $(this).attr('data-course').split(' ');
        sysColor(coulist, 'orange');
    })
    $(".joba").mouseleave(function(){
        var coulist = $(this).attr('data-course').split(' ');
        sysDeColor(coulist);
    }) 
};



